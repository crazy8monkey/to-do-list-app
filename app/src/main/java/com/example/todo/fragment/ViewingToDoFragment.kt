package com.example.todo.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.example.todo.R
import com.example.todo.models.ToDoModel
import com.example.todo.viewmodel.ToDoListViewModel
import com.example.todo.viewmodel.ToDoViewModel
import com.google.android.material.textfield.TextInputLayout

class ViewingToDoFragment: Fragment() {

    private lateinit var currentToDoTitle: TextInputLayout
    private var viewModel: ToDoListViewModel = ToDoListViewModel()
    private var isNewToDo: Boolean = true
    private var viewingToDo: ToDoModel = ToDoModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        viewModel= ViewModelProvider(requireActivity()).get(ToDoListViewModel::class.java)
        return inflater.inflate(R.layout.viewing_todo_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val saveBtn = view.findViewById<Button>(R.id.save_btn)
        currentToDoTitle = view.findViewById<TextInputLayout>(R.id.to_do_title)
        viewModel?.todoLiveData.observe(viewLifecycleOwner
            ) { toDoResponse ->
                if(toDoResponse != null) {
                    isNewToDo = false
                    val currentToDo: ToDoModel? = toDoResponse as? ToDoModel
                    currentToDoTitle.getEditText()?.setText(currentToDo?.title)
                    if (currentToDo != null) {
                        viewingToDo.id = currentToDo.id
                    }
                }
        }

        saveBtn.setOnClickListener { _ ->
            if(isNewToDo) {
                var newToDo: ToDoModel = ToDoModel()
                newToDo.title = currentToDoTitle.getEditText()?.text.toString()
                viewModel.newItem(newToDo)
                Toast.makeText(context, "To Do Added", Toast.LENGTH_LONG).show()
            } else {
                viewingToDo.title = currentToDoTitle.getEditText()?.text.toString()
                viewModel.updateToDo(viewingToDo.id, viewingToDo)
//                activity?.let {
//                    Toast.makeText(it, "To Do Updated", Toast.LENGTH_LONG).show()
//                }
                Toast.makeText(context, "To Do Updated", Toast.LENGTH_LONG).show()
            }
            currentToDoTitle.clearFocus()
            NavHostFragment.findNavController(this@ViewingToDoFragment).navigate(R.id.go_to_todo_list_destination)
        }
    }

}