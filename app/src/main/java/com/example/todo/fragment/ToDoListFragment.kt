package com.example.todo.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.adapters.ToDoAdapter
import com.example.todo.viewmodel.ToDoListViewModel


class ToDoListFragment: Fragment() {

    lateinit var toDoListRecyclerView: RecyclerView
    private var viewModel: ToDoListViewModel = ToDoListViewModel()
    private var toDoListAdapter = ToDoAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        viewModel= ViewModelProvider(requireActivity()).get(ToDoListViewModel::class.java)
        viewModel?.toDoListLiveData()?.observe(viewLifecycleOwner
        ) { toDoListResponse ->
            if (toDoListResponse != null) {
                toDoListAdapter.setToDoList(toDoListResponse)
            }
        }

        return inflater.inflate(R.layout.todolist_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button = view.findViewById<Button>(R.id.new_to_do_btn)

        toDoListRecyclerView = view.findViewById(R.id.to_do_list)
        toDoListRecyclerView.layoutManager = LinearLayoutManager(context)
        toDoListRecyclerView.adapter = toDoListAdapter
        button.setOnClickListener { _ ->
            findNavController(this@ToDoListFragment).navigate(R.id.go_to_viewing_todo_destination)
            viewModel.setItem(null)
        }

        toDoListAdapter.onItemClick = { pos, view ->
            var toDo = toDoListAdapter.getItem(pos)

            when(view.id) {
                R.id.remove_to_do -> {
                    viewModel.removeToDo(toDo.id)
                    Toast.makeText(context, "To Do Removed", Toast.LENGTH_LONG).show()
                }
                R.id.redirect_to_do -> {
                    viewModel.setItem(toDo)
                    findNavController(this@ToDoListFragment).navigate(R.id.go_to_viewing_todo_destination)
                }
            }
        }
    }
}
