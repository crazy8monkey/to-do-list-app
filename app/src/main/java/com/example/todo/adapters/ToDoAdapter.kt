package com.example.todo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.holder.ToDoHolder
import com.example.todo.models.ToDoModel


class ToDoAdapter(): RecyclerView.Adapter<ToDoHolder>() {
    private var currentResults: ArrayList<ToDoModel> = ArrayList<ToDoModel>()
    var onItemClick: ((pos: Int, view: View) -> Unit)? = null

    override fun onCreateViewHolder(parent:ViewGroup, viewType: Int): ToDoHolder {
        var toDoItemInFlated = LayoutInflater.from(parent?.context).inflate(R.layout.to_do, parent, false)
        return ToDoHolder(toDoItemInFlated)
    }

    override fun getItemCount(): Int {
        return currentResults.size
    }

    override fun onBindViewHolder(holder: ToDoHolder, position: Int) {
        var currentToDo = currentResults.get(position)
        holder.onClickEventListener = onItemClick
        holder?.updateToDo(currentToDo)
    }

    fun getItem(position: Int): ToDoModel {
        return currentResults.get(position)
    }

    fun setToDoList(list: ArrayList<ToDoModel>) {
        currentResults = list
        this.notifyDataSetChanged()
    }

}