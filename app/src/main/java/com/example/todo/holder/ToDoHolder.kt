package com.example.todo.holder

import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todo.R
import com.example.todo.models.ToDoModel

class ToDoHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

    private val toDoTitleView: TextView = itemView.findViewById<TextView>(R.id.to_do_title)
    private var currentToDo: ToDoModel? = null

    var onClickEventListener: ((pos: Int, view: View) -> Unit)? = null
    val removeBtn: ImageButton = itemView.findViewById<ImageButton>(R.id.remove_to_do)
    val redirectBtn: ImageButton = itemView.findViewById<ImageButton>(R.id.redirect_to_do)

    init {
        redirectBtn.setOnClickListener(this)
        removeBtn.setOnClickListener(this)
    }

    fun updateToDo(toDo: ToDoModel) {
        currentToDo = toDo
        toDoTitleView.text = toDo.title
    }

    override fun onClick(view: View) {
        onClickEventListener?.invoke(adapterPosition, view)
    }
}
