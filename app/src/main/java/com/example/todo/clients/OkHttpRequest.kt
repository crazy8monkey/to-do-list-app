package com.example.todo.clients

import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request

class OkHttpRequest {
    private val BASE_URL = "http://10.0.2.2:5001/api/"
    var client = OkHttpClient()

    init {
        this.client = client
    }

    fun List(callback: Callback): Call {
        val request = Request.Builder()
            .url(BASE_URL + "todo")
            .build()

        val call = client.newCall(request)
        call.enqueue(callback)
        return call
    }

}