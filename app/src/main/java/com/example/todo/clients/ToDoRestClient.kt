package com.example.todo.clients

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.example.todo.clients.OkHttpSSLClient


class ToDoRestClient {
    private val BASE_URL = "https://10.0.2.2:5001/api/"

    fun getApiClient(): Retrofit {

        val okHttpClient = OkHttpSSLClient()
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient.getUnsafeOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}