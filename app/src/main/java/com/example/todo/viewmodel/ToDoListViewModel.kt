package com.example.todo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.todo.models.ToDoModel
import com.example.todo.repository.ToDoRespository


class ToDoListViewModel: ViewModel() {

    private var todoRepository: ToDoRespository = ToDoRespository()
    private var todoListLiveData: LiveData<ArrayList<ToDoModel>>? = null
    val todoLiveData = MutableLiveData<Any>()

    init {
        todoListLiveData = todoRepository.getToDoData()
    }

    fun toDoListLiveData(): LiveData<ArrayList<ToDoModel>>? {
        return todoListLiveData
    }

    fun setItem(toDo: ToDoModel?) {
        todoLiveData.value = toDo
    }

    fun newItem(toDo: ToDoModel) {
        todoRepository.newToDo(toDo)
    }

    fun removeToDo(id: Long) {
        todoRepository.removeToDo(id)
    }

    fun updateToDo(id: Long, toDo: ToDoModel) {
        todoRepository.updateToDo(id, toDo)
    }
}