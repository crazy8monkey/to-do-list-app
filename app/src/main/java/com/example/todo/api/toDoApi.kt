package com.example.todo.api

import retrofit2.Call
import com.example.todo.apiresponse.ToDoList
import com.example.todo.models.ToDoModel
import com.example.todo.models.RemoveModel
import retrofit2.http.*

interface toDoApi {

    @GET("todo")
    fun getToDoList(): Call<ArrayList<ToDoModel>>

    @GET("todo/{id}")
    fun getToDo(@Path("id") toDoId: Long): Call<ToDoModel>

    @POST("todo")
    fun saveNewToDo(@Body todo: ToDoModel): Call<ToDoModel>

    @DELETE("todo/{id}")
    fun removeToDo(@Path("id") toDoId: Long): Call<RemoveModel>

    @PUT("todo/{id}")
    fun updateToDo(@Path("id") toDoId: Long, @Body todo: ToDoModel): Call<ToDoModel>
}