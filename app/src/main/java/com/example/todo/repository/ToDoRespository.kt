package com.example.todo.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.todo.api.toDoApi
import com.example.todo.clients.ToDoRestClient
import com.example.todo.models.RemoveModel
import com.example.todo.models.ToDoModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ToDoRespository {

    private var restClient = ToDoRestClient().getApiClient().create(toDoApi::class.java)
    private var toDoListLiveData: MutableLiveData<ArrayList<ToDoModel>> = MutableLiveData<ArrayList<ToDoModel>>()

    init {
        restClient.getToDoList().enqueue(object: Callback<ArrayList<ToDoModel>> {
            override fun onResponse(call: Call<ArrayList<ToDoModel>>, response: Response<ArrayList<ToDoModel>>) {
                toDoListLiveData.value = response.body() as ArrayList<ToDoModel>;
            }

            override fun onFailure(call: Call<ArrayList<ToDoModel>>, t: Throwable) {
                Log.v("retrofit error", t.message.toString())
            }
        })
    }

    fun newToDo(data: ToDoModel) {
        restClient.saveNewToDo(data).enqueue(object: Callback<ToDoModel> {
            override fun onResponse(call: Call<ToDoModel>, response: Response<ToDoModel>) {
                var newToDoResponse = response.body() as ToDoModel
                var currentToDos = toDoListLiveData.value
                currentToDos?.add(newToDoResponse)
                toDoListLiveData.value = currentToDos
            }

            override fun onFailure(call: Call<ToDoModel>, t: Throwable) {
                Log.v("retrofit save error", t.message.toString())
            }
        })
    }


    fun removeToDo(id: Long) {
        restClient.removeToDo(id).enqueue(object: Callback<RemoveModel> {
            override fun onResponse(call: Call<RemoveModel>, response: Response<RemoveModel>) {
                var currentToDos = toDoListLiveData.value
                currentToDos?.removeAll {
                    it.id == id
                }
                toDoListLiveData.value = currentToDos
            }

            override fun onFailure(call: Call<RemoveModel>, t: Throwable) {
                Log.v("retrofit remove error", t.message.toString())
            }
        })
    }

    fun updateToDo(id: Long, toDo: ToDoModel) {
        restClient.updateToDo(id, toDo).enqueue(object: Callback<ToDoModel> {
            override fun onResponse(call: Call<ToDoModel>, response: Response<ToDoModel>) {
                var currentToDosList = toDoListLiveData.value
                var toDoIndex = currentToDosList?.indexOfFirst { it.id == id }
                currentToDosList?.get(toDoIndex as Int)?.title = toDo.title

                toDoListLiveData.value = currentToDosList
            }

            override fun onFailure(call: Call<ToDoModel>, t: Throwable) {
                Log.v("retrofit update error", t.message.toString())
            }
        })
    }

    fun getToDoData(): LiveData<ArrayList<ToDoModel>> {
        return toDoListLiveData
    }


}